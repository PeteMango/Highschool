# Unit 1: Science skills

# Unit 2: Chemistry

## Chemistry Vocabulary List

<table class="table" style="max-width:80%">
<tr>
  <th>Word</th>
  <th>Definition (or diagram/translation)</th>
</tr>
<tr>
  <td>Particle Theory of Matter</td>
  <td>Theory that describes the composition and behaviour of matter as being composed of small particles with empty space</td>
</tr>
<tr>
  <td>Matter</td>
  <td>Substance that has mass and occupies space</td>
</tr>
<tr>
  <td>Mechanical Mixture</td>
  <td>A heterogeneous mixture which one can physically separate</td>
</tr>  
<tr>
  <td>Suspension</td>
  <td>A heterogeneous mixture where insoluble solid particles are distributed throughout a fluid, floating freely/td>
</tr>
<tr>
  <td>Alloy</td>
  <td>A combination of 2+ metals</td>
</tr>
<tr>
  <td>Mixture</td>
  <td>A substance that is made up of at least 2 types of particles</td>
</tr>
<tr>
  <td>Qualitative property</td>
  <td>A property of a substance that is not measured and doesn't have a numerical value, such as colour, odour, and texture</td>
</tr>
<tr>
  <td>Qualitative observation</td>
  <td>An numerical observation</td>
</tr>
<tr>
  <td>Precipitate</td>
  <td>A solid that separates from a solution</td>
</tr>
<tr>
  <td>Density</td>
  <td>A measure of how much mass is contained in a given unit volume of a substance; calculated by dividing the mass of a sample of its volume <b>(mass/volume)</b></td>
</tr>    
<tr>
  <td>Element</td>
  <td>Element An element is made up of the same atoms throughout, and cannot be broken down further</td>
</tr>
<tr>
  <td>Metal</td>
  <td>a solid material that is typically hard, shiny, malleable, fusible, and ductile, with good electrical and thermal conductivity</td>
</tr>  
<tr>
  <td>Pure substance</td>
  <td>A substance that is made up of only one type of particle</td>
</tr>  
<tr>
  <td>Atom</td>
  <td>The smallest unit of matter found in substances</td>
</tr>  
<tr>
  <td>Solution</td>
  <td>A uniform mixture of 2 or more substances</td>
</tr>  
<tr>
  <td>Colloid</td>
  <td>is substance with small particles suspended in it, unable to be separated by gravity</td>
</tr>  
<tr>
  <td>Emulsion</td>
  <td>A mixture of 2 insoluble liquids, in which one liquid is suspended in the other</td>
</tr>  
<tr>
  <td>Physical Property</td>
  <td>Characteristic of a substance that can be determined without changing the makeup of the substance</td>
</tr>  
<tr>
  <td>Characteristic</td>
  <td>A physical property that is unique to a substance and can be used to identify the substance</td>
</tr>
<tr>
  <td>Periodic Table</td>
  <td>a table of the chemical elements arranged in order of atomic number, usually in rows, so that elements with similar atomic structure (and hence similar chemical properties) appear in vertical columns.</td>
</tr>
<tr>
  <td>Compound</td>
  <td>Compounds are chemically joined atoms of different elements</td>
</tr>
<tr>
  <td>Non-Metal</td>
  <td>A substance that isn’t a metal</td>
</tr> 
<tr>
<td>Physical Change</td>
<td>A change in which <b>the composition of the substance remains unaltered` and `no new substances are produced</b></td>
</tr>
<tr>
<td>Chemical Change</td> 
<td>A <b>change</b> in the starting substance and the <b>production of ONE or more new substances</b><br> Original substance does not disappear <b>BUT</b> the composition is rearranged</td>
</tr>
<tr>
<td>Molecule</td>
<td>Two or more <b>non-metal</b> atoms joined together</td>
</tr>
<tr>
<td>Diatomic Molecules</td>
<td>Molecules that <b>only</b> consists of 2 elements <br> `H O F BR I N CL` - `hyrodgen`, `oxygen`, `fluorine`, `bromine`, `iodine`, `nitrogen`, `chlorine`.</td>
</tr>
<tr>
<td>Ions</td>
<td>A Charged particle, that results from a <b>loss</b> (cation - positve, less electrons) or <b>gain</b> (anion - negative, more electrons) of electrons when bonding</td>
</tr>
<tr>
<td>Electron</td>
<td>Negatively Charged</td>
</tr>
<tr>
<td>Proton</td>
<td>Positively Charged</td>
</tr>
<tr>
<td>Neutron</td>
<td>Neutral Charged</td>
</tr>
<td>Ionic Charge</td>
<td>The <b>sum</b> of the positive and negative charges in a ion</td>
</tr>
<tr>
<td>Covalent Bond</td>
<td>The sharing of electrons between atoms when bonding</td>
</tr>
<tr>
<td>Valence Electrons</td>
<td>Number of electrons on the most outer orbit/shell of the element</td>
</tr>
</table>

## Particle Theory of Matter
1. Matter is made up of tiny particles.
2. Particles of Matter are in constant motion.
3. Particles of Matter are held together by very strong electrical forces.
4. There are empty spaces between the particles of matter that are very large compared to the particles themselves.
5. Each substance has unique particles that are different from the particles of other substances.

## Physical Properties
- A characeristic of a substance that can be determined without changing the composition ("make-up") of that substance
- Characteristics can be determinded using your 5 senses and measuring instruments 
  - smell, taste, touch, hearing, sight
  - scales, tape, measuring meter
  

## Qualitative and Quantitative Properties
 |Type|Definition|Example|
 |:---|:---------|:------|
 |Quantitative Property|A property that IS measured and has **```a numerical value```** |Ex. **```Temperature, height, mass, density```**|
 |Qualitative Property|A property that is NOT measured and has **```no numerical value```**|Ex. **```Colour, odor, texture```**|

## Density
<img src="https://mathsmadeeasy.co.uk/wp-content/uploads/2017/12/density-mass-volume-triangle.png" width="300">

## Quantitative physical Properties
 - **```Density```**: amount of ```stuff``` (or mass) per unit volume (g/cm<sup>3</sup>)
 - **```Freezing Point```**: point where water solidifies (0<sup>o</sup>C)
 - **```Melting Point```**: point where water liquefies (0<sup>o</sup>C)
 - **```Boiling Point```**: point where liquid phase becomes gaseous (100<sup>o</sup>C)

## Common Qualitative Physical Properties

 |Type|Definition|Example|
 |:---|:---------|:------|
 |Lustre|Shininess of dullness<br> Referred to as high or low lustre depending on the shininess||
 |Clarity|The ability to allow light through|```Transparent``` (Glass) <br>```Translucent``` (Frosted Glass) <br>```Opaque``` (Brick)|
 |Brittleness|Breakability or flexibility<br> Glass would be considered as brittle whereas slime/clay are flexible|
 |Viscosity|The ability of a liquid or gas to resist flow or not pour readily through<br> Refer to as more or less viscous|Molasses is more viscous, water is less (gases tend to get"thicker as heated; liquids get runnier)|
 |Hardness|The relative ability to scratch or be scratched by another substance<br> Referred to as high or low level of hardness| Can use a scale (1 is wax, 10 is diamond)|
 |Malleability|the ability of a substance ```to be hammered``` into a thinner sheet or molded|Silver is malleable<br> Play dough/pizza dough is less<br> glass is not malleable|
 |Ductility|the ability of a substance to be pulled into a finer strand|Pieces of copper can be drawn into thin wires, ductile|
 |Electrical Conductivity|The ability of a substance to allow electric current to pass through it<br> Refer to as high and low conductivity|Copper wires have high conductivity<br> Plastic has no conductivity|
 |Form: Crystalline Solid|Have their particles arranged in an orderly geometric pattern|Salt and Diamonods|
 |Form: Amorphous Solid|Have their particles randomly distributed without any long-range-pattern|Plastic, Glass, Charcoal|

# Chemical Property

- A characteristic (property) of a substance that describes its ability to undergo ```changes to its composition to produce one of more new substances. AKA BEHAVIOUR. Everything has one!```
- ```Cannot be determined by physical properties```
- E.g. ability of nails /cars to rust
- Fireworks are explosive
- Denim is resistant to soap, but is combustible
- Baking soda reacts with vinegar and cake ingredients to rise
- Bacterial cultures convert milk to cheese, grapes to wine, cocoa to chocolate
- CLR used to clean kettles, showerheads because it breaks down minerals
- Silver cleaner for tarnished jewellery, dishes because silver reacts with air to turn black.

## Periodic Table
<img src="https://chem.libretexts.org/@api/deki/files/121316/1.png?revision=1&size=bestfit&width=1300&height=693" width="1200">

### Trends On The Periodic Table
- The first column are the `Alkali metals`.
    - They are shiny, have the consitency of clay, and are easily cut with a knife. 
    - They are the **most reactive** metals.
    - They react violently with water.
    - Alkali metals are **never found as free elements in nature**. They are always bonded with another element.
- The second column are the `Alkaline earth metals`.
    - They are **never found uncombined in nature**.
- The last column are the `Noble gases`.
    - **Extremely un-reactive**. 
- The second last column are the `Halogens`.
    - The **most reactive non-metals**
    - They **react with alkali metals to form salts**.
- The middle parts are the `transition metals`.
    - They are good conductors of heat and electricity.
    - Usually bright coloured.
    - They have properties similar to elements in their same family
    - Many of them combine with oxygen to form compounds called oxides.
- The rows outside the table are the `Inner tranistion metals`.

<img src="https://files.catbox.moe/6522hg.png" width="600">

- The **left** to the **staircase** are the metals and the **right** are the non-metals. The ones touching the **staircase** are the `metalloids`.

<img src="http://www.sussexvt.k12.de.us/science/The%20Periodic%20Table/Periodic%20Trends_files/image002.jpg" width="300"> 

### How To Read An Element
<img src="https://www.classzone.com/books/earth_science/terc/content/investigations/es0501/images/es0501_p6_readinginfo_b.gif" width="400">


## History of The Atom
|Person|Description|Picture|
|:-----|:----------|:------|
|Democritus|All matter can be divided up into smaller pieces until it reaches an unbreakable particle called an ATOM (cannot be cut)<br>He proposed atoms are of diffent sizes, in constant motion and separated by empty spaces||
|Aristole|- Rejected Democritus ideas, believed all matter was made up the 4 elements, it was accepted for nearly 2000 years|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/ce/Four_elements_representation.svg/1227px-Four_elements_representation.svg.png" width="500">|
|John Dalton|- Billbard model, atoms of **different elements are different**<br>Atoms are never **created or destroyed**.<br> - Atoms of an element are identical|<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR-AsCpeBvgYIQMSWuGCG7-Rdb8z5QC9Jb92jnCO_nYkI4snYG7" width="500">|
|JJ Thomson|- Atoms contain negatively charged electrons, since atoms are neutral, the **rest of the atom is a positevly charged sphere**. <br> - Negatively charged electrons were **evenly distrubuted** throughout the atom.<br> - **Ray cathode experiment** - basically atoms were attracted to a postive end of the tube, so there most be negative charges in the atoms. <br> <br> <img src="https://study.com/cimages/multimages/16/thomsonexperiment2.png" width="300">|<img src="https://www.electrical4u.com/images/march16/1468862016.png" width="500">|
|Ernest Rutherford|- Discovered that the postively charged **nucleus**. <br> - The nucleus was **surrounded by a cloud of negatively charged electrons**<br> - Most of the atom was just space. <br> - **Gold foil experiement**, alpha particles (postively charged) shot at atom, some bounced off at weird angles, so there most be a postively charged thing there. <br> <br> <img src="http://historyoftheatom.files.wordpress.com/2015/02/gold-foil.jpg" width="300">|<img src="http://atomicmodeltimelinervmf.weebly.com/uploads/1/7/9/9/17998887/1823773_orig.jpg" width="500">|
|Niels Bohr|- Discovered that electrons **orbit the nucleus in fixed paths**, each electron has a **definite** amount of energy, further from nucles = more energy.<br> - Electrons **cannot** jump orbit to orbit or release energy as light going down. <br> - Each orbit can hold a specifc amount of electrons, `2,8,8,2`, useful for the first 20 elements|<img src="https://s3.amazonaws.com/rapgenius/Bohr%20Atom.png" width="500">|
|James Chadwick|- Discovered the neutron, mass of neutron = mass of proton (basically)<br> - Neutral atoms have **equal numbers** of protons and electrons.|<img src="https://01a4b5.medialib.edu.glogster.com/I28dU77RETpL5o21KLw0/media/43/432f51edf42bbf2082e35268160b789a7344a49f/screen-shot-2014-11-15-at-9-10-48-am.png" width="500">|


## Carbon

## Atoms
- Subscripts - tells us how many of the atom are there, for example N<sub>2</sub> means there are 2 nitrongen atoms.
- Use distrubutive property if there are brackets and a subscript, for example, (CO)<sub>2</sub> is equilivant to C<sub>2</sub>O<sub>2</sub>.
- Atoms are stable if they have a full valence shell (noble gases)
- Each family has the same amount of valence electrons as their family number, so `alkali metals` would have 1 valence electron, `alkaline earth metals` will have 2, `halogens will have` 7 and `noble gases` would have 8. 
- They will also have the same amount of protons as their `atomic number`.
- **Number of protons = Number of electrons**.
- **Number of neutrons = mass - atomic number/number of protons**.


## Bohr-Rutherford / Lewis-Dot Diagrams
- **Bohr-Rutherford**
    - Draw nucleus, and draw the apprioate number of orbits.
    - Put number of **protons** and **neutrons** in the nucleus.
    - Draw the correct number of electrons in each orbit
     <img src="https://d2jmvrsizmvf4x.cloudfront.net/LHJtmeuTDVQ4l2uelrkw_imgres.png" width="300">

- **Lewis-Dot Diagrams**
    - Draw element symbol
    - Put the right number of valence electrons around the symbol, perferably in pairs
    <img src="https://qph.fs.quoracdn.net/main-qimg-a7b2f5ac4b313c27d4bac65c1c8f0a30.webp" width="300">    

### Bonding
- To combine 2 atoms, each element wants to be stable. So they each want a full valence shell, (outer shell) so they are stable.
- They can either `gain`, `lose` or `share` electrons in order to become stable.
- Example:
    - Oxygen and Hydrogen, in order to become stable, they all need 8 valence electrons. Hydrogen has 1, oxygen has 6, so we bring in another hyrdogen and we let them share all their electrons, turning into H<sub>2</sub>O, or water.
     <img src="https://www.seaturtlecamp.com/wp-content/uploads/2014/09/water-molecule-2.gif" width="300">

- Use **arrows** to show gaining or losing electrons.
- **Circle** to show sharing of electrons.



## Naming of Ionic Bonds
1. Write cation (metal) first
2. Write anion (non-metal) second
3. Change the ending of the non-metal to ```ide```.

<img src="https://files.catbox.moe/014ff4.png" width="500">

<br>

<img src="https://files.catbox.moe/3nn8kx.png" width="505">

## Decomposition
  - A chemical change used to break compounds down into simpler substances
  - Energy must be ADDED
    - Using electricity
    - Adding thermal energy
 

## Catalyst
- Substance that accelerates a chemical change without being consumed OR changed itself
  

## Uses of Hydrogen Peroxide
- On cuts/scraps
  - Blood has a catalyst = see bubbling O<sub>2</sub>
- Cleans contact lenses
  - Bubbling removes dirt
- Bleaches
  - React with compounds that provide color
  - RESULT = no colour (bleach blond hair/teeth)
  

# Unit 3: Biology

## Terms
- `Habitat`: Placce where organisms live
- `Biotic`: Living components (their remains AND features)
    - Bears, insects, micro-organisms, nests
- `Abiotic`: Non-living components
    - Physical/chemical components
    - Temperature, wind, humidity, precipitation, minerals, air pressure
- `Sustainability`: **The ability to maintain natural ecological conditions without interruption, weakening, or loss of value.**
- `Community`: Individual from all of the DIFFERENT populations (communities of different species)
- `Ecosystem`: Term given to the community and its interactions with the abiotic environment 
- `Sustainable Ecosystem`: An ecosystem that is maintained through natural processes
- `Ecological niche`: Every species interacts with other species and with its environment in a unique way. This is its role in an ecosystem (e.g. what it eats, what eats it, how it behaves, etc.)
- `Biodiversity`: The variety of life in a particular ecosystem, also known as biological diversity.
    - Canada is home to about 140 000 to 200 000 species of plants and animals. Only 71 000 have been identified.
- `Species Richness`: the number of species in an area.
    - Diverse ecosystem = high species richness.
    - Higher close to the equator.
    - Ex. Amazon rainforest home to more than 200 species of hummingbirds, Ontario only has a single species.
- `Population`: A group of organisms of one species that interbreed and live in the same place and time.
    - Population often change due to both **natural and artifical** factors (human activity). 
- `Carry Capcity`: The maximum population size of a **particular species** that a given ecosystem can sustain.
- `Pollution`: harmful comtaminants released into the enviornment.
- `Bioremediation`: the use of micro-organisms to consume and break down environmental pollutants.
- `Photosynthesis`: The process in which the Sun’s energy (LIGHT) is converted (put together with) into chemical energy AS GLUCOSE (sugar).
- `Succession`: The gradual and usually predictable changes in the composition of a community and the abiotic condtions following a disturbance.
- `Producer`: Organism that makes its own energy-rich food using the Sun’s energy.
- `Consumer`: Organism that obtains its energy from consuming other organisms.
- `Eutrophication`: Overfertilzation of staganat bodies of water with nutrients
- `Heterotrophs` Organisms that feed on others
- `Bioaccumulation`: The process by which **toxins accumulate in the bodies** of animals. (Eg, DDT). **They cannot be easily excreted from the body.**
- `Bioamplification`: The **increase in concentration of a substance** such as a pesticide as we move up trophic level within a food web. **It happens because of bioaccumulation**. (Sometimes called `biomagnification`).
- `Oligotrophic` Bodies of water that are **low** in nutrients. (clear water, opposite to `eutrophic`).
- `Watershed` (drainage basin): Area of land where **ALL WATER** drains to a single river or lake.
- `Invasive Species`: A non-native species whose intentional or accidental introduction negatively impacts the natural environment.
## The Spheres of Earth
### Atmosphere
- The layer of `gases` above Earth's surface, extending upward for hundreds of kilometers.
    - `78% nitrogen gas`.
    - `21% oxygen gas`.
    - `< 1% argon, water vapour, carbon dioxide & other gases`.
- Critical to (almost all) life on Earth.
- Acts like a **blanket & moderates surface temperature**.
- Insulation prevents excessive **heating** during the day & **excessive cooling** during the night.
- Average surface temperature droup from **15C to -18C**.
- Blocks some **solar radiation (most ultraviolet light)**.

    
### Biosphere
- The regions of Earth where `living organisms` exist.
- Describes **the locations in which life can exist within the lithosphere, atmosphere and hydrosphere**.
- Biosphere is thin in comparison to diameter of the Earth.
- ALL conditions required for **life must be met and maintained within this thin layer of ground, water, and nutrients to survive**.

### Hydrosphere
- All the `water` found on Earth, above and below the Earth's surface.
- Includes
    - **Oceans**
    - **Lakes**
    - **Ice**
    - **Ground Water**
    - **Clouds**
- 97% of water on Earth **is in the oceans**.


### Lithosphere
- The `hard part` of Earth's surface.
- **Rocky outer shell of Earth**.
- Consists of:
    - **Rocks and minerals that make up mountains, ocean floors, and Earth's solid landscape**
-Thickness: **50 - 150km**.


## Energy Flow
- `Law of Conservation of Energy`: Energy **can not** be **created** or **destroyed**. It can only be transformed or transfeered.
- Note that Photosynthesis and Cellular respiration are nearly **THE EXACT OPPOSITE**.
### Types of Energy
- #### Radiant Energy
    - Energy that travels through EMPTY SPACE
- #### Thermal Energy
    - Form of energy TRANSFERED DURING HEATING/COOLING
    - Keeps the Earth's surface warm
    - CANNOT provide organisms with energy to grow & function
- #### Light Energy
    -  VISIBLE forms of radiant energy
    - Can be used by some organisms (CANNOT be stored)
- #### Chemical Energy
    - Used by living organisms to perform functions (growth, reproduction, etc.)
    - MUST be replaced as it is used

### Photosynthesis
- Plants use the sun to make energy in the form of glucose or sugar.
- Animals cannot make their own food (glucose, energy)
    - Must get our food from plants.
- Plants are the first step in the food chain
- Oxygen released during photosynthesis is necessary for all living things

### Cellular Respiration
- Process of converting sugar into carbon dioxide, water and energy
- Makes stored energy available for use
- Takes place in the mitochondria

1. Original energy stored in the sugar is released
2. Occurs continuously
3. Does NOT require light energy

- **BOTH** producers **AND** consumers perform cellular respiration

- ALL humans are consumers (unless you’re the hulk)

#### Steps in Cellular Respiration
- Mitochondria takes in nutrients
    - Glucose and Oxygen
- Breaks both nutrients down
    - Creates energy for the cell
- #### REVERSE of Photosynthesis
    - Sugar breaks down into **CARBON DIOXIDE** and **WATER**
        - Release of energy when this happens
    

## Feeding Relationships
- Energy flow through an ecosystem in one direction, from the sun or inorganic compounds to autotrophs (producers) and then to various hetrotrophs (consumers).
- Food are a series of steps in which organisms transfers energy by eating or eaten (pg. 43).
- Food webs show the complex interactions within an ecosystem (pg. 44).
- Each step in a food chain or web is called a `trophic` level. Producers make up the first step, consumers make up the higher levels. E.g. first trophic level are producers, second trophic level are primary consumers, etc.
- Detrivores + scavengers are off to side (with all arrows pointing on it.
- **First Trophic Level**: `Plants`.
- `10% rule`, Only 10% of energy is stored in each organism, 90% of energy is lost (heat consumption).

## Ecological Pyramids
- Food chains and food webs do not give any information about the numbers of organisms involved.
- This information can be shown through ecological pyramids.
- An ecological pyramid is a diagram that shows the amount of energy or matter contained within each trophic level in a food web or food chain.

<img src="https://www.tutorialspoint.com/environmental_studies/images/upright_pyramid.jpg" width="300"> 

|Pyramid|Description|Picture|
|:------|:----------|:------|
|Pyramid of Biomass|Show the **total** amout of `living tissue` available at each `trophic` level. This shows the amount of tissue available for the next `trophic` level. <br> <br> Biomass is preferred to the use of numbers of organisms because individual organisms can vary in size. It is the `total mass` **(not the size)** that is important. Sometimes it’s inverted. <br> <br> Pyramid of biomass records the total dry organic matter of organisms at each trophic level in a given area of an ecosystem.|<img src="http://earth.rice.edu/mtpe/bio/biosphere/topics/energy/biomass_pyramid.gif" width="800">
|Numbers Pyramids|Shows the number of organisms at each trophic level per unit area of an ecosystem. <br> <br> Because each trophic level harvests only about `one tenth` of the energy from the level below, it can support only about one `10th` the amount of living tissue. <br> <br> **`Can be inverted`**: 1 large tree supports thousands of organisms living on it <br> <br> Pyramid of numbers displays the number of individuals annualy.|<img src="https://d321jvp1es5c6w.cloudfront.net/sites/default/files/imce-user-gen/pyramidnumbers2.png" width="400">|
|Energy Pyramid|Shows the amount of energy input to each trophic level in a given area of an ecosystem over an extended period.<br> <br> **CANNOT** be inverted, due to energy transfers<br> <br> **Only 10% of the energy available within one trophic level is transferred to organisms at the next trophic level.**|<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Ecological_Pyramid.svg/1200px-Ecological_Pyramid.svg.png" width="500">|

**NOTE FOR ENERGY PYRAMIDS**: In nature, ecological
efficiency varies from `5%` to `20%` energy available between successive trophic levels (`95%` to `80%` loss). About 10% efficiency is a general rule. `Rule of 10’s` at each level.

## Cycles

|Cycle|Picture|
|:----|:------|
|Water Cycle|<img src="https://www.noaa.gov/sites/default/files/styles/scale_crop_1120x534/public/thumbnails/image/watercycle_rc.png?itok=CcUyhuxd" width="800">|
|Carbon Cycle|<img src="http://climatechangenationalforum.org/wp-content/uploads/2014/09/carbon_cycle_1.jpg" width="700">|
|Nitrogen Cycle|<img src="https://i.pinimg.com/originals/95/e6/d2/95e6d2b0e43e826e2d811d894103d94e.jpg" width="700">|

## Water Cycle
- Continuous movement of water on, above and below the surface of the Earth
### Key Terms:
- Water moves from one reservoir to another (ocean to
atmosphere, river to lake)
    - Evaporation, Condensation, Precipitation, Percolation (Infiltration, water seeping into ground), Run-off, transpiration (plants losing water to air)
    - Forms: Solid (ice), Liquid (water), Gas (vapour)

### STEPS/PROCESS:
- Exchange of energy leads to:
    - Temperature Change, Climate
    - Condenses 🡪 occurs during cooler temp
    - Evaporation 🡪 happens during warmer temp
- **Evaporation**:
    - purifies the water
    - New fresh water for the land
- **Flow of liquid water and ice**
    - Transports minerals across the globe
- **Reshaping the geological features of Earth**
    - Erosion and sedimentation 

### Human Inpacts
- Humans building dams (flooding is a problem!)
- Deforestation contributes to global warming, hence melting glaciers and causing flooding in cities
- (Also less transpiration from clear cutting) – pg. 48
- Factories and cars pollute the air, leading to acid precipitation
- Oil spills destroy aquatic ecosystems

## Carbon Cycle
- Fourth most abundant element in universe
- Building block of all living things
- Main Pathway – in and out of living matter
- 

### STEPS/PROCESSES
- All living organisms contain carbon.
- CO<sub>2</sub> is a waste product of cellular respiration
- Plants use carbon dioxide and water to form simple sugars (photosynthesis)
- Light Energy -->  Chemical Energy
- Carbon dioxide is `returned to the enviornment by:
    1. `Resipiration` in plants, animals & micro-organisms.
    2. `Decay` caused by micro-organisms (decompoers).
    3. `Combustion` i.e. Burning fossil fuels.
- **Phtosynthesis**
    - CO<sub>2</sub> is converted to glucose using water and sunlight
- **Cellular Respiration**
    - Breaks down glucose to release energy, expel CO<sub>2</sub>
- **Oceans are a HUGE carbon sink**.

<img src="https://www.news-medical.net/image.axd?picture=2017%2F6%2Fshutterstock_581980219.jpg" width="500">

### Human Impacts
- **Mining & burning fossil fuels**: Speed up release of CO<sup>2</sub> to the atmosphere.
- **Deforestation & clearing vegetation**:  ↑ CO<sub>2</sub> in atmosphere.
- **Acid rain**: release CO<sub>2</sub> from limestone.
- CO<sub>2</sub> in the atmosphere is now higher than it has been in at least **800 000 years**.

## Nitrogen Cycle
- The most abudant gas in the atmopshere (~78%)
- `Nitrogen Fixation`: The process that causes the strong two-atom nitrogen molecules found in the atmopshere to break apart so they can combine with other atoms.
- `Nitrogen gets fixed`: Whenit is combined with oxygen or hydrogen.
- An essential component of DNA, RNA, and protenis - the building blocks of life.
- Atmopspheric nitrogen = N<sub>2</sub>
    - Most living organisms are `unable` to use this form of nitrogen
    - Therefore, must be **converted** to a usable form!
### STEPS/PROCESSES
<img src="https://image.slidesharecdn.com/lab-11methodsforestimatingdenitrificationprocess-130414125752-phpapp01/95/lab11-methods-for-estimating-denitrification-process-4-638.jpg?cb=1365944316" witdh="100">

### Nitrogen Fixation
- Most of the nitrogen used by living things is taken from the atmosphere by certain bacteria in a process called `nitrogen fixation`. 
- These microorganisms convert nitrogen gas into a variety of nitrogen containing compounds such as nitrates, nitrites, and ammonia
- Lightning and UV radiation also fix small amounts of it
- Humans add nitrogen to soil through fertilizer
- 3 ways nitrogen to get fixed
    1. Atmopheric Fixation
        - Lightning Storms
            - stroms and fuel burning in car engines produce nitrates, which are washed by rain into soil water. 
    2. Industrial Fixation
    3. Biological Fixation
        - 2 types
            1. Free living Bacteria
                - Highly specialized bacteria live in the soil and have the ability to combine atmospheric nitrogen with hydrogen to make ammonium(NH<sub>4</sub><sup>+</sup>).
                - Nitrogen changes into ammonium.
            2. Symbiotic Relationship Bacteria
                - Bacteria live in the roots of legume family plants and provide the plants with ammonium(NH<sub>4</sub><sup>+</sup>) in exchange for the plant's carbon and a protected biome.
- `Nitrites` are absorbed by plant roots and converted to plant protein.
- `Nitrates` **can be absorbed by other plants** to continue the cycle.
- `Denitrifying bacteria` convert soil nitrates into N<sub>2</sub> gas
    - This is a `loss` of N<sub>2</sub> from the cycle 

### Human Impacts
- Nitrates also `enters` the cycle **through the addition of nitrogen rich fertilizers to the soil** – made industrially from nitrogen gas (Eutrophication – pg. 60) 
- Factories release NO compounds (acid rain)

## Nutrient Recycling
- There is a `limited` amount of `nutrients` on earth
    - e.g. you are probably aware of the water cycle – where water is
    constantly being `recycled` in nature.
    - There are similar cycles for all nutrients.
- When plants and animals die, their nutrient content is `not wasted`.
- Bacteria and fungi decompose the remains and release the nutrients back into the abiotic environment (i.e. into the soil, nearby water and air).
- Nutrients are then taken up by other plants and used to make new organic material.
- This material is passed on down the food chains and is reused by all the chain members.
- When death occurs for these members, the nutrients are again returned to the abiotic environment and the cycling of nutrients continues in this circular way.
- This ensures that there is no real longterm drain on the Earth’s nutrients, despite millions of years of plant and animal activity.


## Changes In Population
- The carry capcacity of an ecosystem depends on numerous biotic and abiotic factors.
- These can be classified into two categories.
    1. `Density dependent factors`
    2. `Density independent factors`

### Density Independent Factors
- DIF’s can affect a population no matter what its density is. The effect of the factor (such as weather) on the size of the population **does not** depend on the **original size** of the population.
- Examples:
    - unusual weather
    - natural disasters
    - seasonal cycles
    - certain human activities—such as damming rivers and clear-cutting forests

### Density Dependent Factors
- DDF’s affect a population **ONLY** when it reaches a certain size. The effect of the factor (such as disease) on the size of the population depends on the **original size** of the population
- Examples:
    - Competition
    - Predation
    - Parasitism
    - Disease

## Relationships
1. **Symbiosis**
    - Two different organisms associate with each other in a close way.
    - Is the interaction between members of `two different species` that live together in a close association.
    - **Mutualism (+/+)**
        - Both species benefit from the relationship.
        - (eg. human intestine and good bacteria, bees and flowers, clownfish and sea anemone, cattle egret and cow).
    - **Commensalism (+/0)**
        - one species benefits, the other is **unaffected**. 
        - (eg. beaver cutting down trees, whales and barancles).
    - **Parasitism (-/+)**
        - one species is harmed, the other **benefits**. 
        - (eg. lice and humans, mosquito and humans).
    - **Competition (-/-)**
        - neither species benefits. Can be harmed. (-/-).
    - **Neutralism (0/0)**
        - both species are unaffected (unlikely).
        - True neutralism is extremely unlikely or even impossible to prove. One cannot assert positively that there is absolutely no competition between or benefit to either species.
        - Example: fish and dandelion
     
     <table class="table" style="max-width:80%">
     <tr>
     <th><b>+</b></th>
     <td><b>Parasitism and Predation</b></td>
     <td><b>Commensalism</b></td>
     <td><b>Mutalism</b></td>
     </tr>
     <tr>
     <th><b>0</b></th>
     <td></td>
     <td><b>Neutralism</b></td>
     <td><b>Commensalism</b></td>
     </tr>
     <tr>
     <th><b>-</b></th>
     <td><b>Competition</b></td>
     <td></td>
     <td><b>Parasitism and Predation</b></td>
     </tr>
     <tr>
     <th></th>
     <th><b>-</b></th>
     <th><b>0</b></th>
     <th><b>+</b></th>
     </tr>
     </table> 

2. **Competition**
    - Individuals compete for limited resources
        - Types
            - **Intraspecific Competition**
                - Is the competition between individuals of the **same** species.
                - (eg. male deer uses antlers to fight each other for mates, little herons compete for food).
            - **Interspecific Competition**
                - Is the competition between individuals of **different** species.
                - (eg. cardinals and blue jays at a bird feeder, lions and hyenas competing for food).

3. **Predation**
    - One animal eats (kills) another

### Reasons To Compete
- Food and water.
- Space (habitat).
- Mates.

## Candian Biomes
<img src="https://slideplayer.com/slide/12708159/76/images/41/Canada%E2%80%99s+Biomes+Mountain+Forest+Tundra+Boreal+Forest+Grassland.jpg" width="800">

### Tundra
* Most **NORTHERN** biome of Canada.
* Low temperatures + lots of **PERMAFROST**
* Low decomposition rate.
* Plants grow slower due to cold
* `Species`: Polar bears, Caribou, Arctic foxes.

### Boreal Forest
* **Largest** biome in Canada.
* Warmer weather+plenty rainfall.
* Acidic Soil -  Limits variety of plants + slows down decomposition.
* `Species` Grey wolves, conifers, moss, black bears.

### Grassland
* Moderate rainfall (supports grass not trees).
* Dry $`\rightarrow`$ Fire $`\rightarrow`$ Fire prevents larges trees from growing.
* Very **Fertile** black soil (high decomposition rate)
* Large portions of this biome are replaced by farms in Canada.
* `Species`: Bison, Snakes, fescue grasses, voles.

### Temperate Deciduous Forest
* Layers of canopy trees, understorey trees, shrubs, ground vegetation.
* Variety oof plants + species.
* Fast decomposition rate (warm temperatures).
* Large portions of this biome used by humans for cities.
* `Species`: Shrews, decidious trees, deer, black bears.

### Mountain Forest
* Temperatures vary with elevation
* Windy + cool summers
* Heavy precipitation on leeward side of mountains
* `Species` Elk, cougar, large coniferous trees, ferns.

## Introducing Ecosystems
- Most ecosystems are **SUSTAINABLE**.
### Ecosystem Services
- **Cultural Services**
    -  Benefits relating to our enjoyment of the environment.
    - Ex. Recreational, aesthetic and spiritual experiences when we interact with natural surroundings.
    - Ecotourism: tourists engage in environmentally responsible travel to relatively undisturbed natural areas.
    - Ex. Canada’s Wilderness.
- **Ecosystem Products**
    - Humans use products produced by the ecosystem.
    - Hunt animals and harvest plants, lakes/oceans supply us with seafood.
    - **Terrestrial:** ecosystems: medicines, fibres, rubber and dyes.
    - **Forestry**: largest industries and employers.

- Regulate and maintain important abiotic and biotic features of environment.
    - Cycle water, oxygen, and nutrients.
- Help protect us from physical threats.
    - Plant communities protect the soil from wind and water erosion.
- Ecosystems act as sponges.
    - Absorb water and slowly release it into the groundwater and surface water (reduces erosion and protects against flooding, filters the water).
- Protect land from storms along coasts where wave damage erodes the shoreline.
    - Mangroves

#### Monetary Value of Ecosystem Services

- Very difficult to put a dollar value to it.
- Dollar value of cleaning the air/water, moderating climate and providing paper fibre, medicines and other products is HIGH.
- Ranges into the trillions of dollars/year (maybe 60 trillion?).
- Provides valuable services that are free and renewable.


### Successions
- Natural ecosystems are in a state of equilibrium (their biotic and abiotic features remain relatively constant over time).
- Equilibrium is established when abiotic conditions are stable.
    - Photosynthesis and cellular respiration are balanced.
    - Populations are healthy and stable.
- Small ecosystems are in a constant state of change.
    - Forest fire or disease outbreak can cause short-term changes on a local level.
    - Types
        - #### Primary
            - on newly epxposed  ground, such asa following a volcanic eruption.
        - #### Secondary
            - in a partially distrubed ecosystem, such as following a forest fire. 
            - Human caused disturbances.
        - Results in gradual changes as plants, animals, fungi and micro organisms become established in an area.
        - In both terrestrial and aquatic ecosystems.

#### Benefits of Succession
- Provides a mechanism by which ecosystems maintain their long term sustainability.
- Allows ecosystems to recover from natural or human caused disturbances.
- Offers hope (New Orleans, New Jersey, Florida, Puerto Rico).
- Time needed is very long.
- Original cause of disturbance must be eliminated.
- Not all disturbances can be repaired.
- Disturbances can be repaired through human actions that support the natural processes of succession.


### Human Impacts To Species
- Increase rate of **EXTINCT** species.
- HIPPO
    - Habitat destruction + fragmentation (divide lands into pieces)
    - Invasive species.
    - Pollution + pesticides.
    - Over Population
    - Overexploitation (overfishing, overhunting etc).

#### Status of Endangered Life in Canda
- Do not have to be driven to extinction for there to be ecological consequences.
- Population falls below critical level = ecological niche can no longer be filled.
- Consequences for abiotic and biotic features.
- **Extirpated**: no longer exists in a specific area. 
- **Endangered**: facing imminent extirpation or extinction.
- **Threatened**: likely to become endangered if factors reducing its survival are not changed.
- **Special Concern**: may become threatened or endangered because of a combination of factors.

### Eutrophication
#### The Problem
- Lack or no dissolved oxygen, produces toxic algae, ugly.
- Colour, smell, and water treatment problems.
- Animal waste = nutrients.
- Examples
    - Parks in china.
    - Hanoi, vietnam.

#### Solutions
- Water cycling, through the use of watermills or waterfalls.
- People removing decomposing plants, collecting waste/garabage.
- Aerator.

### Resource Management
#### Forestry Practices
- Canadian economy rely heavily on forests.
- Difficult to find a balance between commerical demands and ecological integrity.
- Forest certifications are given to people that use safe practices
### Wildlife Management
- **`Stewardship`** (sustainable harvesting) must always be remembered!
    - Inuit people had small populations and knew how their enviornments worked, European settlers changed all of that!


## Unit 5 Assessment of Learning
### Derivaties of Trigonometric, Exponential and Logarithmic Functions

#### Question 1:
$`\sin^2(2x) + 4\sin^2(x) = \cos^2(x) + 1`$

$`4\sin^2(x) + 4\sin^2(x)\cos^2(x) - \cos^2(x) - 1 = 0`$ $`\rarr`$ The expression is in the form $`4a + 4ab - b - 1 = 0`$ which can be simplifed to be $`(b+1)(4a-1)`$

$`[\cos^2(x) + 1] \times [4\sin^2(x) - 1] = 0`$

$`[\cos^2(x) + 1] \times [2\sin(x) + 1] \times [2\sin(x) - 1] = 0`$

$`\therefore \cos^2(x) + 1 = 0 \; || \; 2\sin(x) + 1 = 0 \; || \; 2\sin(x) - 1 = 0`$]

---
$`\cos^2(x) + 1 = 0 \rarr`$ **No solution** ($`\cos^2(x) \geq 0`$ for all $`x`$)

$`2\sin(x) + 1 = 0 \rarr \sin(x) = -\frac{1}{2} \rarr x = -\frac{5}{6} \pi, -\frac{1}{6} \pi`$

$`2\sin(x) - 1 = 0 \rarr \sin(x) = \frac{1}{2} \rarr x = \frac{5}{6} \pi, \frac{1}{6} \pi`$

$`\therefore x = \pm \frac{5}{6} \pi, \; \pm \frac{1}{6} \pi`$

#### Question 2:
$`\frac{\cos(2x)-1}{\sin^2(x)+1}=\frac{4\sin^2(x)}{\cos(2x)-3}`$

$`R.S = \frac{4 \sin^2(x)}{\cos(2x)-3}`$

$`= \frac{4[1-\cos^2(x)]}{1-2\sin^2(x)-3}`$

$`= \frac{4 - 4\cos^2(x)}{-2\sin^2(x)-2}`$

$`= \frac{-2[-2 + 2 \cos^2(x)]}{-2[\sin^2(x) + 1]}`$

$`= \frac{2\cos^2(x)-2}{\sin^2(x)+1}`$

$`= \frac{[2\cos^2(x)-1]-1}{\sin^2(x)+1}`$

$`= \frac{\cos(2x)-1}{\sin^2(x) + 1}`$

$`= L.S`$

$`\therefore L.S = R.S`$

#### Question 3:
##### Part A
$` \frac{dy}{dx} (\log_4{\frac{\sin(2^x)}{\tan^3(x^2)}})`$

$`= \frac{dy}{dx} (\frac{\ln {\frac{\sin(2^x)}{\tan^3(x^2)}}}{\ln(4)})`$

$`= \frac{1}{\ln(4)} \frac{dy}{dx} (\ln(\frac{\sin(2^x)}{\tan^3(x^2)}))`$

$`= \frac{1}{\frac{\sin(2^x)}{\tan^3(x^2)}} \frac{dy}{dx} (\frac{\sin(2^x)}{\tan^3(x^2)})`$

$`= \frac{1}{\ln(4)} \cdot \frac{1}{\frac{\sin(2^x)}{\tan^3(x^2)}} \cdot \frac{\ln(2) \cdot 2^x \cos(2^x) \tan(x^2) - 6x \sec^2(x^2)\sin(2^x)}{\tan^4(x^2)}`$

##### Part B
$`\frac{dy}{dx} (\frac{7^{\sin(2x)}}{2\cos(e^{5x})})`$

$`= \frac{1}{2} \frac{dy}{dx} (\frac{7^{\sin(2x)}}{\cos(e^{5x})})`$

$`= \frac{1}{2} [\frac{\frac{dy}{dx} (7^{sin(2x)})\cos(e^{5x})-\frac{dy}{dx} (\cos(e^{5x}))7^{\sin(2x)}}{\cos(e^{5x})^2}]`$

$`= \frac{1}{2}[\frac{2\ln(7) \cdot 7^{\sin(2x)}\cos(2x)\cos(e^{5x})-(-\sin(e^{5x})e^{5x}\cdot 5) \cdot 7^{\sin(2x)}}{(\cos(e^{5x}))^2}]`$

#### Question 4:
$`\log(x^2y)=x-8y`$

$`\frac{2y+x\frac{d}{dx}(y)}{\ln(10)xy} = 1 - 8\frac{d}{dx}(y)`$

$`\frac{d}{dx}(y) = \frac{\ln(10)xy-2y}{x(1+8\ln(10)y)}`$

$`\therefore`$ The slope of the tangent line at point $`(10, 1)`$ is $`\frac{\ln(10) \cdot 10 \cdot 1 - 2 \cdot 1}{10(1 + 8\ln(10) \cdot 1)} = 0.108`$

#### Question 5:
$`y^2 = \sec(x)^{\tan(x)}`$

$`\ln(y^2) = \ln[\sec(x)^{\tan(x)}]`$

$`2 \ln(y) = \tan(x) \ln[\sec(x)]`$

$`\frac{d}{dx}[2\ln(y)] = \frac{d}{dx} [\tan(x) \ln[\sec(x)]]`$

$`(2)\frac{1}{y}(y') = \sec^2(x) \ln[\sec(x)] + \tan^2(x)`$

$`y' = \frac{1}{2} \sqrt{\sec(x)^{\tan(x)}} [\sec^2(x) \ln[\sec(x)]] + \tan^2(x)`$

#### Question 6:
Sketch $`y_1 = \frac{4x^3}{e^x}`$ and $`y_2 = 5 \cos(x)`$. Find the points of intersection.

The function $`y_1 = \frac{4x^3}{e^x}`$ and $`y_2 = 5 \cos(x)`$ intersect at points $`(1.165, 1.973), (5.337, 2.925), (7.686, 0.834)`$

Below is a sketch of the function in the range $`x \in [0, 10]`$

<a href="https://ibb.co/zV423mT"><img src="https://i.ibb.co/zV423mT/IMG-20210115-223355.jpg" alt="IMG-20210115-223355" border="0"></a>

#### Question 7:
Let the height of the triangle be $`h`$ and the base angle be $`\theta`$. 

$`\frac{dh}{dt} = 5 \frac{cm}{min}`$

$`\cos(\theta) = \frac{20}{\sqrt{20^2 + 10^2}}`$

$`\cos(\theta) = \frac{2\sqrt{5}}{5}`$

$`\sec(\theta) = \frac{5}{2\sqrt{5}}`$

$`\tan(\theta) = \frac{h}{20}`$

$`20\tan(\theta) = h`$

$`20 \sec^2(\theta) \frac{d\theta}{dt} = \frac{dh}{dt}`$

When the area is $`200 cm^3`$, the height is $`200 \times 2 \div 40 = 10 cm`$.

$`\because h = 10, \sec(\theta) = \frac{5}{2 \sqrt{5}}`$

$`20(\frac{5}{2 \sqrt{5}}) \cdot \frac{d\theta}{dt} = 5`$

$`\frac{d\theta}{dt} = \frac{5}{25} = \frac{1}{5}`$

$`\therefore`$ the angle is increasing at an rate of $`\frac{1}{5} \frac{rad}{min}`$

#### Question 8:
Let the height of the trapezoid face be $`h`$ and the distance between the corner of the trapezoid until the perpendicular line which intersects the top part of the tent and the bottom part of the tent meet be $`x`$

$`\because \cos(\theta) = \frac{x}{8}, \sin(\theta) = \frac{h}{8}`$

$`\frac{1}{2}[12 + (12 + 16\cos)] 10 \sin(\theta)`$

$`5\sin(\theta)(16\cos(\theta)+24)`$

Inorder to determine the max/min, set $`Area' = 0`$

$`Area' = 5[cos(\theta)(16 \cos(\theta) + 24) - 16\sin^2(\theta)] = 0`$

$`\cos(\theta) = \frac{-3 \pm \sqrt{41}}{8} \; || \; \cos(\theta) = 0`$

$`\cos(\theta) = 0 \rarr \theta = 90^{\circ}`$ (rejected)

$`\cos(\theta) = \frac{-3 + \sqrt(41)}{8} \rarr \theta = 64.82^{\circ}`$

$`\cos(\theta) = \frac{-3 - \sqrt(41)}{8} \rarr D.N.E`$


# Emperical Formula Lesson

1. _Find the Emperical Formula of a compound that is $`85.6 \% C`$ and $`14.4 \% H`$_

|$`Element`$|$`g \; per \; 100 g `$|$`M \; g/mol `$|                    $`n`$                        |       $`Ratio`$         |
|:---------:|:--------------------:|:-------------:|:-----------------------------------------------:|:----------------------: |
|   $`C`$   |      $`85.6 g`$      |  12.01 g/mol  |$`85.6 \; g \times \frac{1 mol}{12.01 g} = 7.13`$| $`\frac{7.13}{7.13}=1`$ |
|   $`H`$   |      $`14.4 g`$      |   1.01 g/mol  |$`14.4 \; g \times \frac{1 mol}{14.4 g} = 14.26`$| $`\frac{14.26}{7.13}=2`$|

$`\therefore`$ _The empirical formula of the compound is_ $`CH_2`$

